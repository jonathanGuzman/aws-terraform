resource "aws_s3_bucket" "b" {
  bucket = "examplejonathanfaz"
  acl = "private"

  tags = {
    Name = "example"
    Environment = "Dev"
  }
}
