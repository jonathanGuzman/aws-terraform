# Terraform
Simple project to make your infrastructure in AWS. 

## Install
You need to have terraform installed on your machine.

To run this example, clone the repository and run terraform apply within the example's own directory.

```sh
$ git clone https://gitlab.com/jonathanGuzman/aws-terraform
$ cd aws-terraform
$ terraform validate terraform-config
$ terraform plan terraform-config
$ terraform apply
```

If you want to use Gitlab CI you have to set the "AWS_ACCESS_KEY" and "AWS_SECRET_KEY" to your repository.
